import { NestFactory } from '@nestjs/core';
import { EventReceiverAndApiModule } from './app/event-receiver-and-api.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(EventReceiverAndApiModule);

  const config = new DocumentBuilder()
    .setTitle('Events Test')
    .setDescription('The Events API description')
    .setVersion('1.0')
    .addTag('events')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(3002);
}
bootstrap();
