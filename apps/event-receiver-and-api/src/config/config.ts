const config = {
  topicName: 'exchange1',
  pupSubRoutingKey: 'events.publish.*.api',
  queue: 'api-service-queue',
  rpcRoutingKey: 'rpc-routing-key',
};

export default config;
