import { Controller, Get } from '@nestjs/common';
import { EventsService } from './events.service';
import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { GetEventDto } from '../dto/get-event.dto';
import config from '../config/config';

@Controller('events')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @RabbitSubscribe({
    exchange: config.topicName,
    routingKey: config.pupSubRoutingKey,
    queue: config.queue,
  })
  public async eventsHandler(event: GetEventDto) {
    this.eventsService.logEvents(event);
  }

  @Get()
  getRecentEvents(): Promise<GetEventDto[]> {
    return this.eventsService.getRecentEvents();
  }
}
