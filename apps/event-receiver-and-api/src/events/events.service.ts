import { Injectable } from '@nestjs/common';
import { GetEventDto } from '../dto/get-event.dto';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import config from '../config/config';

@Injectable()
export class EventsService {
  constructor(private readonly amqpConnection: AmqpConnection) {}

  logEvents(event: GetEventDto) {
    console.log(`Received Event: ${JSON.stringify(event)}`);
  }

  async getRecentEvents(): Promise<GetEventDto[]> {
    const recentEvents = await this.amqpConnection.request<GetEventDto[]>({
      exchange: config.topicName,
      routingKey: config.rpcRoutingKey,
      timeout: 10000,
    });

    return recentEvents;
  }
}
