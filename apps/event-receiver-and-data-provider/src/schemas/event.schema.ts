import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type EventDocument = HydratedDocument<Event>;

@Schema({
  timestamps: true,
})
export class Event {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  description: string;
}

export const EventSchema = SchemaFactory.createForClass(Event);
