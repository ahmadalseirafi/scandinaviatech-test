import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Event } from '../schemas/event.schema';
import { CreateEventDto } from '../dto/create-event.dto';

@Injectable()
export class EventsRepository {
  constructor(@InjectModel(Event.name) private eventModel: Model<Event>) {}

  async createEvent(createEventDto: CreateEventDto): Promise<Event> {
    const createdEvent = new this.eventModel(createEventDto);
    return createdEvent.save();
  }

  async getRecentEvents(): Promise<Event[]> {
    const latestEvents = await this.eventModel
      .find()
      .sort({ createdAt: -1 })
      .limit(10)
      .exec();

    return latestEvents;
  }
}
