import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventsService } from './events.service';
import { EventsController } from './events.controller';
import { Event, EventSchema } from '../schemas/event.schema';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { EventsRepository } from './events.repository';
import config from '../config/config';
import * as dotenv from 'dotenv';
dotenv.config();

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Event.name, schema: EventSchema }]),
    RabbitMQModule.forRoot(RabbitMQModule, {
      exchanges: [
        {
          name: config.topicName,
          type: 'topic',
        },
      ],
      uri: process.env.rabbitmqURl,
      enableControllerDiscovery: true,
      connectionInitOptions: { wait: false },
    }),
  ],
  controllers: [EventsController],
  providers: [EventsService, EventsRepository],
})
export class EventsModule {}
