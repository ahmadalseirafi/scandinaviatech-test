import { Injectable } from '@nestjs/common';
import { Event } from '../schemas/event.schema';
import { CreateEventDto } from '../dto/create-event.dto';
import { EventsRepository } from './events.repository';

@Injectable()
export class EventsService {
  constructor(private eventsRepository: EventsRepository) {}
  async createEvent(createEventDto: CreateEventDto): Promise<Event> {
    return this.eventsRepository.createEvent(createEventDto);
  }
  async getRecentEvents(): Promise<Event[]> {
    return this.eventsRepository.getRecentEvents();
  }
}
