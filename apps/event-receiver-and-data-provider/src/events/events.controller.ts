import { Controller, Get } from '@nestjs/common';
import { EventsService } from './events.service';
import { Event } from '../schemas/event.schema';
import { RabbitRPC, RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { CreateEventDto } from '../dto/create-event.dto';
import config from '../config/config';

@Controller('events')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @RabbitSubscribe({
    exchange: config.topicName,
    routingKey: config.pupSubRoutingKey,
    queue: config.queue,
  })
  public async eventsHandler(event: CreateEventDto) {
    await this.eventsService.createEvent(event);
  }

  @RabbitRPC({
    routingKey: config.rpcRoutingKey,
    exchange: config.topicName,
    queue: config.rpcQueue,
  })
  getRecentEvents(): Promise<Event[]> {
    return this.eventsService.getRecentEvents();
  }
}
