const config = {
  topicName: 'exchange1',
  pupSubRoutingKey: 'events.publish.data.*',
  queue: 'data-service-queue',
  rpcRoutingKey: 'rpc-routing-key',
  rpcQueue: 'rpc-queue',
};
export default config;
