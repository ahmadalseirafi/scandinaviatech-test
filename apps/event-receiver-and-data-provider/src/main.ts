import { NestFactory } from '@nestjs/core';
import { EventReceiverAndDataProviderModule } from './app/event-receiver-and-data-provider.module';
import { ClusterService } from './cluster/cluster.service ';

async function bootstrap() {
  const app = await NestFactory.create(EventReceiverAndDataProviderModule);
  await app.listen(3001);
}

ClusterService.clusterize(bootstrap);
