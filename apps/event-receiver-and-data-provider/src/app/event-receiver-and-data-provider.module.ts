import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventsModule } from '../events/events.module';
import * as dotenv from 'dotenv';
dotenv.config();

@Module({
  imports: [MongooseModule.forRoot(process.env.dataBaseURl), EventsModule],
  controllers: [],
  providers: [],
})
export class EventReceiverAndDataProviderModule {}
