import { EventsService } from './events.service';
import { Controller, Get } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

@Controller('events')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @Cron('* * * * * *')
  publishEvent() {
    return this.eventsService.publishEvent();
  }
}
