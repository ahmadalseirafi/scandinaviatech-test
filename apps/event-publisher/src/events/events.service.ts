import { Injectable } from '@nestjs/common';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import config from '../config/config';

@Injectable()
export class EventsService {
  constructor(private readonly amqpConnection: AmqpConnection) {}
  public async publishEvent() {
    const date = new Date();
    const eventName = date.toISOString() + ' name';
    const eventDescription = date.toISOString() + ' description';
    await this.amqpConnection.publish(
      config.topicName,
      config.pupSubRoutingKey,
      { name: eventName, description: eventDescription },
    );
  }
}
