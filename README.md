for this application to work some configs must be set fisrt :

in the .env file you have to set the database url and rabbitmq url

this application has 3 microservices :

1 - Event Publisher Microservice:

to run this Microservice use :

npm run start:dev event-publisher


2 - Event Receiver & Data Provider Microservice

to run this Microservice use :

npm run start:dev event-receiver-and-data-provider


3 - Event Receiver & API Microservice

to run this Microservice use :

npm run start:dev event-receiver-and-api


to try the endpoint that gets the most 10 recent events you can use swagger on http://localhost:3002/api

each Microservice has a config object located in the config folder
this object contains the topic , routing keys , queues and rpc config